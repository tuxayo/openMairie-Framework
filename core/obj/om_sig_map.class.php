<?php
/**
 *
 *
 * @package openmairie_exemple
 * @version SVN : $Id$
 */

//
require_once "../gen/obj/om_sig_map.class.php";

/**
 *
 */
class om_sig_map_core extends om_sig_map_gen {

    /**
     * On active les nouvelles actions sur cette classe.
     */
    var $activate_class_action = true;

    /**
     *
     */
    function setType(&$form,$maj) {
        parent::setType($form,$maj);
        $crud = $this->get_action_crud($maj);
        
        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType('projection_externe','select');
            $form->setType('om_sig_extent','autocomplete');
            if($this->retourformulaire=='') {
                $form->setType('sld_marqueur','upload');
                $form->setType('sld_data','upload');
            } else {
                $form->setType('sld_marqueur','upload2');
                $form->setType('sld_data','upload2');
			}
            $form->setType("fond_default", "select");
        }
        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType('projection_externe','select');
            $form->setType('om_sig_extent','autocomplete');
            if($this->retourformulaire=='') {
                $form->setType('sld_marqueur','upload');
                $form->setType('sld_data','upload');
            } else {
                $form->setType('sld_marqueur','upload2');
                $form->setType('sld_data','upload2');
			}
            $form->setType("fond_default", "select");
        }
        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType('sld_marqueur','hiddenstatic');
            $form->setType('sld_data','hiddenstatic');
            $form->setType('sld_marqueur','filestatic');
            $form->setType('sld_data','filestatic');
            $form->setType("fond_default", "selectstatic");
        }
        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType('sld_marqueur','hiddenstatic');
            $form->setType('sld_data','hiddenstatic');
            $form->setType('sld_marqueur','file');
            $form->setType('sld_data','file');
            $form->setType("fond_default", "selectstatic");
        }
    }

    /**
     * Methode verifier
     */
    function verifier($val = array(), &$db = NULL, $DEBUG = false) {
        // On appelle la methode de la classe parent
        parent::verifier($val, $db, $DEBUG);
        // On verifie si le champ n'est pas vide
        if ($this->valF['id'] == "") {
            $this->correct = false;
            $this->addToMessage(_("Le champ")." "._("id")." "._("est obligatoire"));
        } else {
            // On verifie si il y a un autre id 'actif' pour la collectivite
            if ($this->valF['actif'] == true) {
                if ($this->maj == 0) {
                    //
                    $this->verifieractif($db, $val, $DEBUG, ']');
                } else {
                    //
                    $this->verifieractif($db, $val, $DEBUG, $val['om_sig_map']);
                }
            }
        }                
        switch ($this->valF["fond_default"]) {
            case null:
                break;
            case "osm":
                if ($this->valF['fond_osm']!=true) {
                    $this->addToMessage(_("Le fond osm ne peut être choisi par défaut"));
                    $this->correct = false;
                }
                break;
            case "bing":
                if ($this->valF['fond_bing']!=true) {
                    $this->addToMessage(_("Le fond bing ne peut être choisi par défaut"));
                    $this->correct = false;
                }
                break;
            case "sat":
                if ($this->valF['fond_sat']!=true) {
                    $this->addToMessage(_("Le fond sat ne peut être choisi par défaut"));
                    $this->correct = false;
                }
                break;
            default:
                $sql="";
                if ($this->valF["om_sig_map"] != "") {
                    $sql=$this->valF["om_sig_map"];
                    if ($this->valF["source_flux"] != "")
                        $sql.=",".$this->valF["source_flux"];
                
                } else {
                    if ($this->valF["source_flux"] != "")
                        $sql=$this->valF["source_flux"];
                }
                $sql="SELECT count(*) FROM (SELECT DISTINCT om_sig_map_flux FROM ".DB_PREFIXE."om_sig_map_flux where om_sig_map in (".$sql."))  a where om_sig_map_flux=".$this->valF["fond_default"];
                $nb= $db -> getOne($sql);
                if ($nb==0) {
                    $this->addToMessage(_("Ce flux ne peut être choisi par défaut"));
                    $this->correct = false;                
                }                
                break;
        }
    }

    /**
     *
     */
    function setTaille(&$form,$maj) {
        parent::setTaille($form,$maj);
        //taille des champs affiches (text)
        $form->setTaille('om_sig_map',4);
        $form->setTaille('om_collectivite',4);
        $form->setTaille('id',20);
        $form->setTaille('libelle',50);
        $form->setTaille('zoom',3);
        $form->setTaille('fond_osm',1);
        $form->setTaille('fond_bing',1);
        $form->setTaille('fond_sat',1);
        $form->setTaille('etendue',60);
        $form->setTaille('projection_externe',20);
        $form->setTaille('retour',50);
    }

    /**
     *
     */
    function setMax(&$form,$maj) {
        parent::setMax($form,$maj); 
        $form->setMax('om_sig_map',4);
        $form->setMax('om_collectivite',4);
        $form->setMax('id',50);
        $form->setMax('libelle',50);
        $form->setMax('zoom',3);
        $form->setMax('fond_osm',1);
        $form->setMax('fond_bing',1);
        $form->setMax('fond_sat',1);
        $form->setMax('etendue',60);
        $form->setMax('projection_externe',60);
        $form->setMax('url',2);
        $form->setMax('retour',50);
    }

    /**
     *
     */
    function setOnchange(&$form,$maj) {
        parent::setOnchange($form,$maj);
        $form->setOnchange('zoom','VerifNum(this)');
    }

    /**
     *
     */
    function setSelect(&$form, $maj, &$db = null, $debug = null) {
        parent::setSelect($form, $maj,$db,$debug);
        if($maj<2){
            // On définit une valeur par défaut surchargeable par le script
            // dyn/var_sig.inc pour la liste à choix de la projection externe
            $contenu_epsg = array(
                0 => array("", "EPSG:2154", "EPSG:27563", ),
                1 => array("choisir la projection", 'lambert93', 'lambertSud', ),
            );
            if (file_exists("../dyn/var_sig.inc")) {
                include "../dyn/var_sig.inc";
            }
            $form->setSelect("projection_externe", $contenu_epsg);
            //
			$params = array(
				"constraint" => array(
					"extension" => ".sld"
				),
			);
			$form->setSelect("sld_marqueur",$params);
        }// fin maj
        
        //
        if ($maj < 2) {
            //
            $params = array();
            // Surcharge visée pour l'ajout
            $params['obj'] = "om_sig_extent";
            // Table de l'objet
            $params['table'] = "om_sig_extent";
            // Permission d'ajouter
            $params['droit_ajout'] = false;
            // Critères de recherche
            $params['criteres'] = array(
                "om_sig_extent.nom" => _("nom")
            );
            // Tables liées
            $params['jointures'] = array();
            // Colonnes ID et libellé du champ
            // (si plusieurs pour le libellé alors une concaténation est faite)
            $params['identifiant'] = "om_sig_extent.om_sig_extent";
            $params['libelle'] = array (
                "om_sig_extent.nom"
            );
            // Envoi des paramètres
            $form->setSelect("om_sig_extent", $params);
        }
        if ($maj == 0) {
            $sql_fond_default=
                "SELECT  unnest(string_to_array('osm;bing;sat', ';'::text)) as code, ".
                " unnest(string_to_array('osm;bing;sat', ';'::text)) as lib ";
            
        } else {
             $sql_fond_default=
                "SELECT * FROM (SELECT 	mf.om_sig_map_flux::text as code, mf.ol_map as libelle ".
                "FROM 	".DB_PREFIXE."om_sig_map_flux mf ".
                "JOIN 	".DB_PREFIXE."om_sig_flux fl ".
                "	ON mf.om_sig_flux = fl.om_sig_flux ".
                "WHERE	mf.baselayer IS TRUE AND mf.om_sig_map IN (SELECT distinct unnest(string_to_array(om_sig_map||';'||coalesce(source_flux,om_sig_map), ';'::text))::integer FROM ".DB_PREFIXE."om_sig_map WHERE om_sig_map=".$this->val[0].") ".
                "UNION ".
                "SELECT  unnest(string_to_array('osm;bing;sat', ';'::text)) as code, ".
                " unnest(string_to_array('osm;bing;sat', ';'::text)) as lib ) a order by code";
            
        }
        $sql_fond_default_by_id=
            "SELECT * from (SELECT 	mf.om_sig_map_flux::text as code, mf.ol_map as libelle ".
            "FROM 	".DB_PREFIXE."om_sig_map_flux mf ".
            "JOIN 	".DB_PREFIXE."om_sig_flux fl ".
            "	ON mf.om_sig_flux = fl.om_sig_flux ".
            "WHERE	mf.baselayer IS TRUE ". // AND mf.om_sig_map IN (SELECT unnest(string_to_array(om_sig_map||';'||source_flux, ';'::text))::integer FROM ".DB_PREFIXE."om_sig_map WHERE om_sig_map=<idx>) ".
            "UNION ".
            "SELECT  unnest(string_to_array('osm;bing;sat', ';'::text)) as code, ".
            " unnest(string_to_array('osm;bing;sat', ';'::text)) as lib) a ".
            " WHERE code = '<idx>'";
        $this->init_select($form, $this->f->db, $maj, null, "fond_default", $sql_fond_default, $sql_fond_default_by_id, false);
    }

    /**
     *
     */
    function setGroupe (&$form, $maj) {
        
		$form->setGroupe('id','D');
        $form->setGroupe('libelle','G');
        $form->setGroupe('actif','F');
		$form->setGroupe('util_idx','D');
        $form->setGroupe('util_reqmo','G');
        $form->setGroupe('util_recherche','F');
        
        $form->setGroupe('zoom','D');
        $form->setGroupe('fond_osm','G');
        $form->setGroupe('fond_bing','G');
        $form->setGroupe('fond_sat','G');
        $form->setGroupe('layer_info','F');
        
        $form->setGroupe('etendue','D');
        $form->setGroupe('projection_externe','F');
   
		$form->setGroupe('sld_marqueur','D');
        $form->setGroupe('sld_data','F');     
		
    }

    /**
     *
     */
    function setRegroupe (&$form, $maj) {
        
        $form->setRegroupe('id','D',' '._('titre').' ', "collapsible");
        $form->setRegroupe('libelle','G','');
        $form->setRegroupe('actif','F','');
        
		$form->setRegroupe('util_idx','D',' '._('Cas d\'utilisation').' ', "collapsible");
        $form->setRegroupe('util_reqmo','G','');
        $form->setRegroupe('util_recherche','G','');
        $form->setRegroupe('source_flux','F','');
        
		$form->setRegroupe('zoom','D',' '._('fond').' ', "collapsible");
        $form->setRegroupe('fond_osm','G','');
        $form->setRegroupe('fond_bing','G','');
        $form->setRegroupe('fond_sat','G','');
        $form->setRegroupe('layer_info','G','');
        $form->setRegroupe('fond_default','F','');
        
		       
        $form->setRegroupe('om_sig_extent','D',' '._('Paramètres cartographiques').' ', "collapsible");
        $form->setRegroupe('restrict_extent','G','');
        $form->setRegroupe('point_centrage','G','');
        $form->setRegroupe('projection_externe','F','');
        
        $form->setRegroupe('url','D',' '._('marqueurs').' ', "collapsible");
        $form->setRegroupe('om_sql','F','');
		
        $form->setRegroupe('sld_marqueur','D',' '._('SLD').' ', "collapsible");
        $form->setRegroupe('sld_data','F','');
		
      }

    /**
     *
     */
    function setLib(&$form,$maj) {
        parent::setLib($form,$maj);
        //libelle des champs
        $form->setLib('util_idx',_('à partir d\'un enregistrement donné : '));
        $form->setLib('util_recherche',_('d\'une recherche : '));
        $form->setLib('util_reqmo',_('d\'une requète mémorisée : '));
        $form->setLib('source_flux',_('Flux en provedance de la carte : '));
        $form->setLib('fond_default',_('Fond par défaut : '));
        $form->setLib('fond_osm',_('osm : '));
        $form->setLib('fond_bing',_('bing : '));
        $form->setLib('fond_sat',_('sat : '));
        $form->setLib('om_sig_extent',_('étendue'));
        $form->setLib('restrict_extent',_('Restreindre la navigation sur l\'étendue'));
        $form->setLib('point_centrage',_('Centrer sur le point (si différent du centre de l\'étendue)'));
        $form->setLib('projection_externe',_('projection'));
        $form->setLib('url',_('url'));
        $form->setLib('om_sql',_('requete sql'));
        $form->setLib('sld_marqueur',_('Fichier SLD pour les marqueurs'));
        $form->setLib('sld_data',_('Fichier SLD pour les données (om_sig_map_comp)'));
    }

    /**
     *
     */
    function setVal(&$form, $maj, $validation, &$db = null, $DEBUG = null) {
        parent::setVal($form, $maj, $validation, $db, $DEBUG);
        $this->maj=$maj;
    }

    /**
     *
     */
    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$db = null, $DEBUG = null) {
        parent::setValsousformulaire($form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, $db, $DEBUG);
        $this->maj=$maj;
    }

    /**
     * verification sur existence d un etat deja actif pour la collectivite
     */
    function verifieractif(&$db, $val, $DEBUG,$id){
		$sql = "select om_sig_map from ".DB_PREFIXE."om_sig_map where id ='".$val['id']."'";
        $sql.= " and om_collectivite ='".$val['om_collectivite']."'";
        $sql.= " and actif =true";
        $sql.= " and ( 1<>1";
		if ($val['util_idx']=="Oui") $sql.= " OR util_idx IS TRUE";
		if ($val['util_reqmo']=="Oui") $sql.= " OR util_reqmo IS TRUE";
		if ($val['util_recherche']=="Oui") $sql.= " OR util_recherche IS TRUE";
        $sql.= " )";
        if($id!=']')
            $sql.=" and  om_sig_map !='".$id."'";
        $res = $db->query($sql);
        if($DEBUG==1) echo $sql;
        if (database::isError($res))
           die($res->getMessage(). " => Echec  ".$sql);
        else{
           $nbligne=$res->numrows();
           if ($nbligne>0){
               $this->addToMessage($nbligne." "._("sig_map")." "._("existant").
               " "._("actif")." ! "._("vous ne pouvez avoir qu un sig_map")." '".
               $val['id']."' "._("actif")."  "._("par collectivite et par cas d utilisation"));
               $this->correct=False;
            }
        }
    }

}

?>
