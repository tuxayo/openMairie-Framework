<?php
/**
 * Ce fichier permet de declarer la classe database
 *
 * @package openmairie_exemple
 * @version SVN : $Id$
 */

/**
 *
 */
(defined("PATH_OPENMAIRIE") ? "" : define("PATH_OPENMAIRIE", ""));
require_once PATH_OPENMAIRIE."om_debug.inc.php";
(defined("DEBUG") ? "" : define("DEBUG", PRODUCTION_MODE));
require_once PATH_OPENMAIRIE."om_logger.class.php";

/**
 *
 */
require_once "DB.php";

/**
 *
 */
class database extends DB {

    /**
     *
     */
    static function isError($resource = NULL, $forcereturn = false) {
        
        //
        if (!DB::isError($resource)) {
            return false;
        }

        // Logger
        $temp = explode('[', $resource->getDebugInfo());
        if (trim($temp[0]) != "") {
            logger::instance()->log(__METHOD__."(): QUERY => ".$temp[0], DEBUG_MODE);
        }
        logger::instance()->log(__METHOD__."(): SGBD ERROR => ".substr($temp[1], 0, strlen($temp[1])-1), DEBUG_MODE);
        logger::instance()->log(__METHOD__."(): PEAR ERROR => ".$resource->getMessage(), DEBUG_MODE);

        //
        if ($forcereturn == true) {
            return true;
        }
        
        //
        $class = "error";
        $message = _("Erreur de base de donnees. Contactez votre administrateur.");
        //
        echo "\n<div class=\"message ui-widget ui-corner-all ui-state-highlight ui-state-".$class."\">\n";
        echo "<p>\n";
        echo "\t<span class=\"ui-icon ui-icon-info\"><!-- --></span> \n\t";
        echo "<span class=\"text\">";
        echo $message;
        echo "</span>";
        echo "\n</p>\n";
        echo "</div>\n";
        
        //
        echo "</div>";
        die();
        
    }
    
}

?>
