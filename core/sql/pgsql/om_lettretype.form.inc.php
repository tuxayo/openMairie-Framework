<?php
/**
 *
 *
 * @package openmairie_exemple
 * @version SVN : $Id$
 */

//
include "../gen/sql/pgsql/om_lettretype.form.inc.php";

//
$champs=array(
    "om_lettretype",
    "om_collectivite",
    "om_lettretype.id",
    "om_lettretype.libelle",
    "actif",
    "orientation",
    "format",
    "logo",
    "logoleft",
    "logotop",
    "margeleft",
    "margetop",
    "margeright",
    "margebottom",
    "header_offset",
    "header_om_htmletat",
    "titre_om_htmletat",
    "titreleft",
    "titretop",
    "titrelargeur",
    "titrehauteur",
    "titrebordure",
    "corps_om_htmletatex",
    "se_font",
    "se_couleurtexte",
    "footer_offset",
    "footer_om_htmletat",
    "om_sql",
    "'' as merge_fields",
    "'' as substitution_vars"
);

$sql_om_sousetat="select id, coalesce(libelle, id) as libelle from ".DB_PREFIXE."om_sousetat";
$sql_om_sousetat.=" where actif IS TRUE and om_collectivite=".$_SESSION['collectivite'];
$sql_om_sousetat.=" order by libelle";

$sql_om_sousetat_by_id="select id,coalesce(libelle, id) as libelle from ".DB_PREFIXE."om_sousetat";
$sql_om_sousetat_by_id.=" ";

$sql_om_logo="select id, (libelle||' ('||id||')') as libelle from ".DB_PREFIXE."om_logo";
$sql_om_logo.=" where actif IS TRUE and om_collectivite=".$_SESSION['collectivite'];
$sql_om_logo.=" order by libelle";

$sql_om_logo_by_id="select id, (libelle||' ('||id||')') as libelle from ".DB_PREFIXE."om_logo";
$sql_om_logo_by_id.=" WHERE id = '<idx>'";

?>
