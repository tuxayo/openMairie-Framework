<?php
//$Id$ 
//gen openMairie le 10/04/2015 12:03

include "../gen/sql/pgsql/om_sig_flux.inc.php";
$table=DB_PREFIXE."om_sig_flux inner join ".DB_PREFIXE.
        "om_collectivite on om_collectivite.om_collectivite = om_sig_flux.om_collectivite";
$champAffiche=array('om_sig_flux',
                    'om_sig_flux.libelle',
                    'id',
                    "(om_collectivite.libelle||' ('||om_collectivite.niveau||')') as collectivite",
					" CASE WHEN cache_type = 'IMP' THEN 'Impression' WHEN cache_type = 'TCF' THEN 'flux tilecache' WHEN cache_type = 'SMT' THEN 'Slippy Map Tiles' ELSE 'WMS' END as type"
					);
$tri=' order by om_sig_flux.id';

?>
