<?php
/**
 *
 *
 * @package openmairie_exemple
 * @version SVN : $Id$
 */

//
include "../gen/sql/pgsql/om_sig_map.form.inc.php";

//
$champs=array(
    "om_sig_map",
    "id",
    "om_collectivite",
    "libelle",
    "actif",
    "util_idx",
    "util_reqmo",
    "util_recherche",
    "source_flux",
    "zoom",
    "fond_osm",
    "fond_bing",
    "fond_sat",
    "layer_info",
    "fond_default",
    "om_sig_extent",
    "restrict_extent",
    "point_centrage",
    "projection_externe",
    "url",
    "om_sql",
    "sld_marqueur",
    "sld_data",
    "retour"
);

//
//$sql_geometry = "select f_table_name,(f_table_name||' '||srid) as lib from geometry_columns order by f_table_name ";
//$sql_geometry_champ = "select f_geometry_column,(f_table_name||' '||f_geometry_column) as lib from geometry_columns order by f_table_name ";

?>
