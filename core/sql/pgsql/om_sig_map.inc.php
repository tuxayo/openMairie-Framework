<?php
/**
 *
 *
 * @package openmairie_exemple
 * @version SVN : $Id$
 */

//
include "../gen/sql/pgsql/om_sig_map.inc.php";
// FROM 
$table = DB_PREFIXE."om_sig_map
    LEFT JOIN ".DB_PREFIXE."om_collectivite 
        ON om_sig_map.om_collectivite=om_collectivite.om_collectivite 
    LEFT JOIN ".DB_PREFIXE."om_sig_extent 
        ON om_sig_map.om_sig_extent=om_sig_extent.om_sig_extent 
    LEFT JOIN ".DB_PREFIXE."om_sig_map s
        ON om_sig_map.source_flux=s.om_sig_map ";
// SELECT 
$champAffiche = array(
    'om_sig_map.om_sig_map as "'._("Ident.").'"',
    'om_sig_map.id as "'._("id").'"',
    'om_sig_map.libelle as "'._("Libellé").'"',
    "case om_sig_map.actif when 't' then 'Oui' else 'Non' end as \""._("Act")."\"",
    "case om_sig_map.util_idx when 't' then 'Oui' else 'Non' end as \""._("Idx")."\"",
    "case om_sig_map.util_reqmo when 't' then 'Oui' else 'Non' end as \""._("ReqMo")."\"",
    "case om_sig_map.util_recherche when 't' then 'Oui' else 'Non' end as \""._("Rec.")."\"",
    's.libelle as "'._("Src.Flux").'"',
    'om_sig_map.fond_default as "'._("Fond").'"',
	'om_sig_extent.nom as "'._("Etendue").'"',
    );
//
if ($_SESSION['niveau'] == '2') {
    array_push($champAffiche, "om_collectivite.libelle as \""._("collectivite")."\"");
}
//
$champNonAffiche = array(
    'om_sig_map.zoom as "'._("zoom").'"',
    'om_sig_map.fond_osm as "'._("fond_osm").'"',
    'om_sig_map.fond_bing as "'._("fond_bing").'"',
    'om_sig_map.fond_sat as "'._("fond_sat").'"',
    'om_sig_map.layer_info as "'._("layer_info").'"',
    'om_sig_map.etendue as "'._("etendue").'"',
    'om_sig_map.projection_externe as "'._("projection_externe").'"',
    'om_sig_map.retour as "'._("retour").'"',
    'om_sig_map.om_collectivite as "'._("om_collectivite").'"',
    'om_sig_map.url as "'._("url").'"',
    'om_sig_map.om_sql as "'._("om_sql").'"',
    );
//
$champRecherche = array(
    'om_sig_map.om_sig_map as "'._("om_sig_map").'"',
    'om_sig_map.id as "'._("id").'"',
    'om_sig_map.libelle as "'._("libelle").'"',
    'om_sig_map.libelle as "'._("source_flux").'"',
    'om_sig_map.zoom as "'._("zoom").'"',
    'om_sig_map.fond_osm as "'._("fond_osm").'"',
    'om_sig_map.fond_bing as "'._("fond_bing").'"',
    'om_sig_map.fond_sat as "'._("fond_sat").'"',
    'om_sig_map.layer_info as "'._("layer_info").'"',
    'om_sig_map.fond_default as "'._("fond_default").'"',
    'om_sig_map.etendue as "'._("etendue").'"',
    'om_sig_map.projection_externe as "'._("projection_externe").'"',
    'om_sig_map.retour as "'._("retour").'"',
    );
$tri="ORDER BY om_sig_map.libelle ASC NULLS LAST";

?>
