<?php
/**
 *
 *
 * @package openmairie_exemple
 * @version SVN : $Id$
 */

//
include "../gen/sql/pgsql/om_sig_map_comp.inc.php";

$champAffiche = array(
    'om_sig_map_comp.om_sig_map_comp as "'._("Ident.").'"',
    'om_sig_map.id as "'._("Id.map").'"',
    'om_sig_map_comp.libelle as "'._("Libellé").'"',
    'om_sig_map_comp.obj_class as "'._("Obj.").'"',	
    'om_sig_map_comp.ordre as "'._("Ord.").'"',
    "case om_sig_map_comp.actif when 't' then 'Oui' else 'Non' end as \""._("Act.")."\"",
    "case om_sig_map_comp.comp_maj when 't' then 'Oui' else 'Non' end as \""._("Maj.")."\"",
    'om_sig_map_comp.comp_table_update||\'.\'||om_sig_map_comp.comp_champ_idx||\'(\'||om_sig_map_comp.comp_champ||\'->\'||om_sig_map_comp.type_geometrie||\')\' as "'._("Req.").'"',
    );
//
$champNonAffiche = array(
    'om_sig_map_comp.comp_champ_idx as "'._("comp_champ_idx").'"',
    );
$tri="ORDER BY om_sig_map_comp.ordre ASC NULLS LAST";





?>
