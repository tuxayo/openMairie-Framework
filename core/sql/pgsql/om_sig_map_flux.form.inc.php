<?php
//$Id$ 
//gen openMairie le 10/04/2015 12:03

include "../gen/sql/pgsql/om_sig_map_flux.form.inc.php";
$champs=array(
    "om_sig_map_flux",
    "om_sig_flux",
    "om_sig_map",
    "ol_map",
    "baselayer",
    "maxzoomlevel",
	"ordre",
    "visibility",
    "singletile",
    "panier",
    "pa_nom",
    "pa_layer",
    "pa_attribut",
    "pa_encaps",
    "pa_sql",
    "pa_type_geometrie",
    "sql_filter");

//champs select
$sql_om_sig_map="SELECT om_sig_map.om_sig_map, om_sig_map.libelle FROM ".DB_PREFIXE."om_sig_map ORDER BY om_sig_map.libelle ASC";
$sql_om_sig_map_by_id = "SELECT om_sig_map.om_sig_map, om_sig_map.libelle FROM ".DB_PREFIXE."om_sig_map WHERE om_sig_map = <idx>";
$sql_om_sig_flux="SELECT om_sig_flux.om_sig_flux, om_sig_flux.libelle||' - '||CASE WHEN cache_type IS NULL OR cache_type='' THEN 'WMS' ELSE cache_type END||CASE WHEN length(cache_gfi_chemin||cache_gfi_couches) > 0 THEN '*' ELSE '.' END FROM ".DB_PREFIXE."om_sig_flux ORDER BY om_sig_flux.libelle ASC";
$sql_om_sig_flux_by_id = "SELECT om_sig_flux.om_sig_flux, om_sig_flux.libelle||' - '||CASE WHEN cache_type IS NULL OR cache_type='' THEN 'WMS' ELSE cache_type END||CASE WHEN length(cache_gfi_chemin||cache_gfi_couches) > 0 THEN '*' ELSE '.' END FROM ".DB_PREFIXE."om_sig_flux WHERE om_sig_flux = <idx>";

?>
