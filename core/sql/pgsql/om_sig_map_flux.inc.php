<?php
//$Id$ 
//gen openMairie le 10/04/2015 12:03

include "../gen/sql/pgsql/om_sig_map_flux.inc.php";
$champAffiche = array(
    'om_sig_map_flux.om_sig_map_flux as "'._("id").'"',
    'om_sig_map.libelle as "'._("om_sig_map").'"',
    'om_sig_flux.libelle||\' (\'||CASE WHEN cache_type IS NULL OR cache_type = \'\' THEN \'WMS\' ELSE cache_type END||\')\'  as "'._("flux").'"',
    'om_sig_map_flux.ol_map as "'._("nom OL").'"',
	"case om_sig_map_flux.baselayer when 't' then 'Oui' else 'Non' end as \""._("Base")."\"",
    'om_sig_map_flux.ordre as "'._("ordre").'"',
    "case om_sig_map_flux.visibility when 't' then 'Oui' else 'Non' end as \""._("Vis.")."\"",
    "case om_sig_map_flux.panier when 't' then 'Oui' else 'Non' end as \""._("panier")."\"",
    );
//
$champNonAffiche = array(
    'om_sig_map_flux.pa_nom as "'._("pa_nom").'"',
    'om_sig_map_flux.pa_layer as "'._("pa_layer").'"',
    'om_sig_map_flux.pa_sql as "'._("pa_sql").'"',
    'om_sig_map_flux.pa_attribut as "'._("pa_attribut").'"',
    'om_sig_map_flux.pa_encaps as "'._("pa_encaps").'"',
    'om_sig_map_flux.pa_type_geometrie as "'._("pa_type_geometrie").'"',
    'om_sig_map_flux.sql_filter as "'._("sql_filter").'"',
    "case om_sig_map_flux.singletile when 't' then 'Oui' else 'Non' end as \""._("singletile")."\"",
    'om_sig_map_flux.maxzoomlevel as "'._("maxzoomlevel").'"',
    );
//
$champRecherche = array(
    'om_sig_map_flux.om_sig_map_flux as "'._("id").'"',
    'om_sig_map.libelle as "'._("om_sig_map").'"',
    'om_sig_flux.libelle||\' (\'||CASE WHEN cache_type IS NULL OR cache_type = \'\' THEN \'WMS\' ELSE cache_type END||\')\'  as "'._("flux").'"',
    'om_sig_map_flux.ol_map as "'._("nom OL").'"',
    'om_sig_map_flux.baselayer as "'._("base").'"',
    'om_sig_map_flux.ordre as "'._("ordre").'"',
    'om_sig_map_flux.visibility as "'._("vis.").'"',
    'om_sig_map_flux.panier as "'._("panier").'"',
    'om_sig_map_flux.pa_nom as "'._("pa_nom").'"',
    'om_sig_map_flux.pa_layer as "'._("pa_layer").'"',
    'om_sig_map_flux.pa_sql as "'._("pa_sql").'"',
    'om_sig_map_flux.pa_attribut as "'._("pa_attribut").'"',
    'om_sig_map_flux.pa_encaps as "'._("pa_encaps").'"',
    'om_sig_map_flux.pa_type_geometrie as "'._("pa_type_geometrie").'"',
    'om_sig_map_flux.sql_filter as "'._("sql_filter").'"',
    "case om_sig_map_flux.singletile when 't' then 'Oui' else 'Non' end as \""._("singletile")."\"",
    'om_sig_map_flux.maxzoomlevel as "'._("maxzoomlevel").'"',
    );

//
$tri="ORDER BY om_sig_map_flux.baselayer,om_sig_map_flux.ordre, om_sig_flux.libelle ";


?>
