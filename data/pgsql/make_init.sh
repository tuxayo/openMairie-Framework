#! /bin/bash
##
# Ce script permet de générer les fichiers sql d'initialisation de la base de
# données pour permettre de publier une nouvelle version facilement
#
# @package openmairie_exemple
# @version SVN : $Id$
##

schema="openexemple"
database="openexemple"

# Génération du fichier init.sql
sudo su postgres -c "pg_dump --column-inserts -s -O -n $schema -t $schema.om_* $database" > init.sql

# Génération du fichier init_metier.sql
sudo su postgres -c "pg_dump --column-inserts -s -O -n $schema -T $schema.om_* $database" > init_metier.sql

# Génération du fichier init_parametrage.sql
sudo su postgres -c "pg_dump --column-inserts -a -O -n $schema -t $schema.om_collectivite -t $schema.om_parametre -t $schema.om_profil -t $schema.om_droit -t $schema.om_utilisateur $database" > init_parametrage.sql

# Génération du fichier init_data.sql
# On ne veut aucune donnée lors de la publication d'une version
# sudo su postgres -c "pg_dump --column-inserts -a -O -n $schema -t $schema.om_logo -t $schema.om_requete -t $schema.om_sousetat -t $schema.om_etat -t $schema.om_lettretype -t $schema.om_sig_flux -t $schema.om_sig_map -t $schema.om_sig_map_comp -t $schema.om_sig_map_flux $database" > init_data.sql

# Suppression du schéma
sed -i "s/CREATE SCHEMA $schema;/-- CREATE SCHEMA $schema;/g" init*.sql
sed -i "s/^SET/-- SET/g" init*.sql

