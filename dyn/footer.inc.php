<?php
/**
 * Ce fichier permet de configurer les liens presents dans le footer
 *
 * @package openmairie_exemple
 * @version SVN : $Id$
 */

/**
 * $footer est le tableau associatif qui contient tous les liens presents dans
 * le footer de l'application
 *
 * Caracteristiques :
 * --- footer
 *     - title [obligatoire]
 *     - description (texte qui s'affiche au survol de l'element)
 *     - href [obligatoire] (contenu du lien href)
 *     - class (classe css qui s'affiche sur l'element)
 *     - right (droit que l'utilisateur doit avoir pour visionner cet element)
 *     - target (pour ouvrir le lien dans une nouvelle fenetre)
 */
$footer = array();

// Template
/*
$footer[] = array(
    "title" => _(""),
    "description" => _(""),
    "href" => "",
    "target" => "",
    "class" => "",
);
*/

// Documentation du site
$footer[] = array(
    "title" => _("Documentation"),
    "description" => _("Acceder a l'espace documentation de l'application"),
    "href" => "http://docs.openmairie.org/?project=framework&version=4.6",
    "target" => "_blank",
    "class" => "footer-documentation",
);

// Forum openMairie
$footer[] = array(
    "title" => _("Forum"),
    "description" => _("Espace d'échange ouvert du projet openMairie"),
    "href" => "https://communaute.openmairie.org/c/framework",
    "target" => "_blank",
    "class" => "footer-forum",
);

// Portail openMairie
$footer[] = array(
    "title" => _("openMairie.org"),
    "description" => _("Site officiel du projet openMairie"),
    "href" => "http://www.openmairie.org/",
    "target" => "_blank",
    "class" => "footer-openmairie",
);

?>
