<?php
/**
 * Ce fichier permet de stocker la version de l'application
 *
 * @package openmairie_exemple
 * @version SVN : $Id$
 */

/**
 * Le numero de version est affiche en permanence dans le footer
 */
$version = "4.6.3.dev0";

?>
