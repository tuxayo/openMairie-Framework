<?php
//$Id$ 
//gen openMairie le 17/02/2017 12:11

$DEBUG=0;
$serie=15;
$ent = _("administration")." -> "._("om_droit");
if(!isset($premier)) $premier='';
if(!isset($recherche1)) $recherche1='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($selectioncol)) {
    $selectioncol = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
if (!isset($recherche)) {
    $recherche = '';
}
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".strtoupper($idz)."&nbsp;";
}
// FROM 
$table = DB_PREFIXE."om_droit
    LEFT JOIN ".DB_PREFIXE."om_profil 
        ON om_droit.om_profil=om_profil.om_profil ";
// SELECT 
$champAffiche = array(
    'om_droit.om_droit as "'._("om_droit").'"',
    'om_droit.libelle as "'._("libelle").'"',
    'om_profil.libelle as "'._("om_profil").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'om_droit.om_droit as "'._("om_droit").'"',
    'om_droit.libelle as "'._("libelle").'"',
    'om_profil.libelle as "'._("om_profil").'"',
    );
$tri="ORDER BY om_droit.libelle ASC NULLS LAST";
$edition="om_droit";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "om_profil" => array("om_profil", ),
);
// Filtre listing sous formulaire - om_profil
if (in_array($retourformulaire, $foreign_keys_extended["om_profil"])) {
    $selection = " WHERE (om_droit.om_profil = ".intval($idxformulaire).") ";
}

?>