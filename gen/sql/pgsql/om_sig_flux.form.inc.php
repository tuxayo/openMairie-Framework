<?php
//$Id$ 
//gen openMairie le 17/02/2017 12:11

$DEBUG=0;
$ent = _("administration")." -> "._("om_sig_flux");
$tableSelect=DB_PREFIXE."om_sig_flux";
$champs=array(
    "om_sig_flux",
    "libelle",
    "om_collectivite",
    "id",
    "attribution",
    "chemin",
    "couches",
    "cache_type",
    "cache_gfi_chemin",
    "cache_gfi_couches");
//champs select
$sql_om_collectivite="SELECT om_collectivite.om_collectivite, om_collectivite.libelle FROM ".DB_PREFIXE."om_collectivite ORDER BY om_collectivite.libelle ASC";
$sql_om_collectivite_by_id = "SELECT om_collectivite.om_collectivite, om_collectivite.libelle FROM ".DB_PREFIXE."om_collectivite WHERE om_collectivite = <idx>";
?>