<?php
//$Id$ 
//gen openMairie le 17/02/2017 12:11

$DEBUG=0;
$ent = _("administration")." -> "._("om_sig_map_flux");
$tableSelect=DB_PREFIXE."om_sig_map_flux";
$champs=array(
    "om_sig_map_flux",
    "om_sig_flux",
    "om_sig_map",
    "ol_map",
    "ordre",
    "visibility",
    "panier",
    "pa_nom",
    "pa_layer",
    "pa_attribut",
    "pa_encaps",
    "pa_sql",
    "pa_type_geometrie",
    "sql_filter",
    "baselayer",
    "singletile",
    "maxzoomlevel");
//champs select
$sql_om_sig_flux="SELECT om_sig_flux.om_sig_flux, om_sig_flux.libelle FROM ".DB_PREFIXE."om_sig_flux ORDER BY om_sig_flux.libelle ASC";
$sql_om_sig_flux_by_id = "SELECT om_sig_flux.om_sig_flux, om_sig_flux.libelle FROM ".DB_PREFIXE."om_sig_flux WHERE om_sig_flux = <idx>";
$sql_om_sig_map="SELECT om_sig_map.om_sig_map, om_sig_map.libelle FROM ".DB_PREFIXE."om_sig_map ORDER BY om_sig_map.libelle ASC";
$sql_om_sig_map_by_id = "SELECT om_sig_map.om_sig_map, om_sig_map.libelle FROM ".DB_PREFIXE."om_sig_map WHERE om_sig_map = <idx>";
?>