<?php
//$Id$ 
//gen openMairie le 17/02/2017 12:11

$DEBUG=0;
$serie=15;
$ent = _("administration")." -> "._("om_sig_map_flux");
if(!isset($premier)) $premier='';
if(!isset($recherche1)) $recherche1='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($selectioncol)) {
    $selectioncol = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
if (!isset($recherche)) {
    $recherche = '';
}
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".strtoupper($idz)."&nbsp;";
}
// FROM 
$table = DB_PREFIXE."om_sig_map_flux
    LEFT JOIN ".DB_PREFIXE."om_sig_flux 
        ON om_sig_map_flux.om_sig_flux=om_sig_flux.om_sig_flux 
    LEFT JOIN ".DB_PREFIXE."om_sig_map 
        ON om_sig_map_flux.om_sig_map=om_sig_map.om_sig_map ";
// SELECT 
$champAffiche = array(
    'om_sig_map_flux.om_sig_map_flux as "'._("om_sig_map_flux").'"',
    'om_sig_flux.libelle as "'._("om_sig_flux").'"',
    'om_sig_map.libelle as "'._("om_sig_map").'"',
    'om_sig_map_flux.ol_map as "'._("ol_map").'"',
    'om_sig_map_flux.ordre as "'._("ordre").'"',
    "case om_sig_map_flux.visibility when 't' then 'Oui' else 'Non' end as \""._("visibility")."\"",
    "case om_sig_map_flux.panier when 't' then 'Oui' else 'Non' end as \""._("panier")."\"",
    'om_sig_map_flux.pa_nom as "'._("pa_nom").'"',
    'om_sig_map_flux.pa_layer as "'._("pa_layer").'"',
    'om_sig_map_flux.pa_attribut as "'._("pa_attribut").'"',
    'om_sig_map_flux.pa_encaps as "'._("pa_encaps").'"',
    'om_sig_map_flux.pa_type_geometrie as "'._("pa_type_geometrie").'"',
    "case om_sig_map_flux.baselayer when 't' then 'Oui' else 'Non' end as \""._("baselayer")."\"",
    "case om_sig_map_flux.singletile when 't' then 'Oui' else 'Non' end as \""._("singletile")."\"",
    'om_sig_map_flux.maxzoomlevel as "'._("maxzoomlevel").'"',
    );
//
$champNonAffiche = array(
    'om_sig_map_flux.pa_sql as "'._("pa_sql").'"',
    'om_sig_map_flux.sql_filter as "'._("sql_filter").'"',
    );
//
$champRecherche = array(
    'om_sig_map_flux.om_sig_map_flux as "'._("om_sig_map_flux").'"',
    'om_sig_flux.libelle as "'._("om_sig_flux").'"',
    'om_sig_map.libelle as "'._("om_sig_map").'"',
    'om_sig_map_flux.ol_map as "'._("ol_map").'"',
    'om_sig_map_flux.ordre as "'._("ordre").'"',
    'om_sig_map_flux.pa_nom as "'._("pa_nom").'"',
    'om_sig_map_flux.pa_layer as "'._("pa_layer").'"',
    'om_sig_map_flux.pa_attribut as "'._("pa_attribut").'"',
    'om_sig_map_flux.pa_encaps as "'._("pa_encaps").'"',
    'om_sig_map_flux.pa_type_geometrie as "'._("pa_type_geometrie").'"',
    'om_sig_map_flux.maxzoomlevel as "'._("maxzoomlevel").'"',
    );
$tri="ORDER BY om_sig_flux.libelle ASC NULLS LAST";
$edition="om_sig_map_flux";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "om_sig_flux" => array("om_sig_flux", ),
    "om_sig_map" => array("om_sig_map", ),
);
// Filtre listing sous formulaire - om_sig_flux
if (in_array($retourformulaire, $foreign_keys_extended["om_sig_flux"])) {
    $selection = " WHERE (om_sig_map_flux.om_sig_flux = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - om_sig_map
if (in_array($retourformulaire, $foreign_keys_extended["om_sig_map"])) {
    $selection = " WHERE (om_sig_map_flux.om_sig_map = ".intval($idxformulaire).") ";
}

?>