<?php
/**
 * Ce script permet d'interfacer la vue 'form_sig'.
 *
 * @package openmairie_exemple
 * @version SVN : $Id$
 */

require_once "../obj/utils.class.php";
$f = new utils("nohtml");
$f->view_form_sig();

?>
