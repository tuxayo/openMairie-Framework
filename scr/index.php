<?php
/**
 * Ce script permet de faire une redirection vers le fichier index.php à la
 * racine de l'application.
 *
 * @package openmairie_exemple
 * @version SVN : $Id$
 */

header("Location: ../index.php");
exit();

?>
