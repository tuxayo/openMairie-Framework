<?php
/**
 * Ce script permet d'interfacer la connexion de l'utilisateur.
 *
 * @package openmairie_exemple
 * @version SVN : $Id$
 */

require_once "../obj/utils.class.php";
$f = new utils("login");
$f->view_login();

?>
