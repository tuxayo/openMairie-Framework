<?php
/**
 * Ce script permet d'interfacer la déconnexion de l'utilisateur.
 *
 * @package openmairie_exemple
 * @version SVN : $Id$
 */

require_once "../obj/utils.class.php";
$f = new utils("logout");
$f->view_login();

?>
