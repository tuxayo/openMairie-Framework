<?php
/**
 * Ce script permet d'interfacer le changement de mot de passe d'un utilisateur.
 *
 * @package openmairie_exemple
 * @version SVN : $Id$
 */

require_once "../obj/utils.class.php";
$f = new utils("nohtml");
$f->view_password();

?>
