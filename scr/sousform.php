<?php
/**
 * Ce script permet d'interfacer le module form de l'application.
 *
 * @package openmairie_exemple
 * @version SVN : $Id$
 */

require_once "../obj/utils.class.php";
$f = new utils("nohtml");
$f->view_sousform();

?>
