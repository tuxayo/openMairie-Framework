<?php
/**
 * Ce script permet de charger les données
 * des établissements pour les autocomplete
 *
 * @package openaria
 * @version SVN : $Id$
 */

// Fichier requis
require_once "../obj/utils.class.php";

//
if (!isset($f)) {
    $f = new utils("nohtml");
    $f->disableLog();
    header("Content-type: text/html; charset=".CHARSET."");
}

// Récupération de la permission d'ajouter
$droit_ajout = (isset($_GET['droit'])) ? $_GET['droit'] : "";
$droit_ajout = ($droit_ajout == "ok") ? true : false;

// Récupération de l'identifiant
$idx = (isset($_GET['idx'])) ? $_GET['idx'] : "";

// Récupération des colonnes ID et libellé
$colid = (isset($_GET['colid'])) ? $_GET['colid'] : "";
$collib = (isset($_GET['collib'])) ? explode(";",$_GET['collib']) : "";
$nb_collib = count($collib);

// Récupération des champs de critère
$criteres = (isset($_GET['criteres'])) ? explode(",",$_GET['criteres']) : "";
$nb_criteres = count($criteres);

// Récupération des tables jointes
$jointures = (isset($_GET['jointures'])) ? explode(",",$_GET['jointures']) : "";

// Récupération de la table de l'objet
$table = (isset($_GET['table'])) ? $_GET['table'] : "";

//
$where = (isset($_GET['where'])) ? $_GET['where'] : "";

// Récupération des champs du group by
$group_by = (isset($_GET['group_by'])) ? explode(",",$_GET['group_by']) : "";

// SELECT
$sql_select = "SELECT ".$colid.", concat(";
$i = 1;
foreach ($collib as $col) {
    $sql_select .= $col;
    if ($i < $nb_collib) {$sql_select .= ",' - ',";}
    $i++;
}
$sql_select .= ") ";
$sql_select .= "FROM ".DB_PREFIXE.$table." ";

// S'il y a des jointures
if (!empty($jointures)) {
    //
    foreach ($jointures as $jointure) {
        //
        $sql_select .= "LEFT JOIN ".DB_PREFIXE.$jointure." ";
    }
}

// WHERE
if (isset($_GET['term'])) {
    //
    $terms = trim(str_replace("-", " ", $_GET['term']));
    $term = explode(" ", $terms);
    $sql_where = " WHERE ";
    //
    foreach ($term as $key => $t) {
        if ($key != 0) {
            $sql_where .= " and ";
        }
        $sql_where .= "(";
        //
        $j = 1;
        foreach ($criteres as $critere) {
            // Normalisation de la valeur recherchée
            $t_temp = normalizeSearchValue($t);
            // Normalisation du champ concerné par la recherche
            $critere_temp = normalizeFieldValue($critere);
            //
            $sql_where .= "(lower(".$critere_temp."::text) like ".$t_temp.")";
            if ($j < $nb_criteres) {$sql_where .= " OR ";}
            $j++;
        }
        $sql_where .= ")";    
    }
}
//
if ($idx != "" && $colid != "") {
    $sql_where = " WHERE ".$colid." = '".$idx."' ";
}
//
if (!empty($where)) {
    //
    $sql_where .= " AND ".$where;
}
//
$sql_group_by = " GROUP BY ".$colid;
//
if (!empty($group_by)) {
    //
    $sql_group_by = " GROUP BY ";
    //
    foreach ($group_by as $value) {
        //
        $sql_group_by .= " ".$value.",";
    }
    //
    $sql_group_by = substr($sql_group_by, 0, -1);
}
//
$sql_limit = " limit 20";

// Exécution de la requete
$sql = $sql_select.$sql_where.$sql_group_by.$sql_limit;
$res = $f->db->query($sql);
$f->addToLog("app/autocomplete.php: db->query(\"".$sql."\");", VERBOSE_MODE);
$f->isDatabaseError($res);

// Construction de la liste de résultats
$output = array();
if ($row = &$res->fetchRow()) {
    do {
        $retour = array();
        $retour['value'] = $row[0];
        $retour['label'] = $row[1];
        array_push($output, $retour);
    } while ($row = &$res->fetchRow());
} else {
    $retour = array();
    $retour['value'] = "";
    $retour['label'] = _("Aucun resultat");
    array_push($output, $retour);
}
if ($droit_ajout) {
    $retour = array();
    $retour['value'] = "";
    $retour['label'] = _("Creer un nouvel enregistrement");
    array_push($output, $retour);
}
echo json_encode($output);

/**
 * Méthode permettant de faire les traitements d'échappements
 * et de normalisation sur une chaîne destinée à la recherche.
 *
 * @param string $value Valeur à rechercher
 *
 * @return string Valeur normalisée
 */
function normalizeSearchValue($value) {

    // échappement des caractères spéciaux
    if (!get_magic_quotes_gpc()) {
        $value = pg_escape_string($value);
    }
    // jokers
    $value = "'%".$value."%'";
    // encodage
    if(DBCHARSET!='UTF8' and HTTPCHARSET=='UTF-8'){
        $value = utf8_decode($value);
    }
    // normalisation des caractères
    $value = " lower(translate(".$value."::varchar,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyaaaaaceeeeiiiinooooouuuuy'))";
    return $value;
}

/**
 * Méthode permettant de normaliser les valeurs des champs de recherche
 * en vue de les comparer aux valeurs recherchées.
 *
 * @param string $searchField Champ sur lequel la recherche est faite
 *
 * @return string Champ normalisé
 */
function normalizeFieldValue($searchField) {
    return "lower(translate(".$searchField."::varchar,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyaaaaaceeeeiiiinooooouuuuy'))";
}

?>
