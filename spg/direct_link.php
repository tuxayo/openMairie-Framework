<?php
/**
 * Script permettant de definir les parametres necessaire a
 * l'acces d'un sous formulaire depuis un autre objet
 *
 * @param obj string : objet de l'objet parent
 * @param action string : action sur l'objet parent
 * @param idx string (optionnel soit idx soit direct_field) : identifiant de
 *                   l'objet contexte
 * @param direct_field string (optionnel soit idx soit direct_field) : nom du
 *                            champ contenant l'identifiant de l'objet contexte
 * @param direct_form string : nom de l'objet du sous form a afficher
 * @param direct_action string : action a effectuer sur le sous-form
 * @param direct_idx mixed : id de l'objet du sous-form a afficher
 *
 * @package openmairie_exemple
 * @version SVN : $Id$
 */

//Instantiation d'om
require_once "../obj/utils.class.php";
$f = new utils("nohtml");
//$f->disableLog();
//Recuperation des valeurs du GET
$param = $_GET;

// Vérification des paramètres obligatoires
if (isset($param['obj']) and !empty($param['obj'])
    and isset($param['action']) and !empty($param['action'])
    and (
        (isset($param['idx']) and !empty($param['idx']))
        or (isset($param['direct_field']) and !empty($param['direct_field']))
    )
    and isset($param['direct_form']) and !empty($param['direct_form'])
    and isset($param['direct_action']) and !empty($param['direct_action'])
    and isset($param['direct_idx']) and !empty($param['direct_idx'])) {
    
    //Verification de la presence de la classe
    if (strpos($param['direct_form'], "/") !== false
        or !file_exists("../obj/".$param['direct_form'].".class.php")) {
        $class = "error";
        $message = _("L'objet est invalide.");
        $f->addToMessage($class, $message);
        $f->setFlag(NULL);
        $f->display();
        die();
    }
    if (file_exists("../sql/pgsql/".$param['obj'].".inc.php")) {
        $obj = $param['obj'];
        require_once "../sql/pgsql/".$param['obj'].".inc.php";
    }

	// Recuperation de l'id de l'onglet
    $tabs_id=0;
    foreach($sousformulaire as $souform) {
        $droit=array();
        $droit[]=$souform;
        $droit[]=$souform."_tab";
        
        if($f->isAccredited($droit,"OR")) {
            $tabs_id++;
            if($souform==$param['direct_form']) {
                break;
            }
        }
    }

    // Pour récupérer l'identifiant de l'objet contexte, deux possibilités :
    // - soit le paramètre identifiant du contexte *idx* est fourni et on prend
    //   directement cet identifiant pour composer l'URL vers le script
    //   ``scr/form.php``,
    // - soit le paramètre identifiant du contexte *idx* n'est pas fourni,
    //   alors on utilise le paramètre *direct_field* pour récupérer la valeur
    //   du champ correspondant dans l'instance de l'objet direct.
    $context_idx = null;
    if (isset($param["idx"]) && !empty($param['idx'])) {
        $context_idx = $param["idx"];
    } else {
        require_once "../obj/".$param['direct_form'].".class.php";
        $object = new $param['direct_form']($param['direct_idx'], $f->db, 0);
        $context_idx = $object->val[array_search($param['direct_field'], $object->champs)];
    }

    //Appel du sous-form avec l'id du formulaire parent recupere dans les valeur de l'objet instancie
    header("Location: ../scr/form.php?obj=".$param['obj'].
                                        "&action=".$param['action'].
                                        "&idx=".$context_idx.
                                        "&direct_form=".$param['direct_form'].
                                        "&direct_idx=".$param['direct_idx'].
                                        "&direct_action=".$param['direct_action'].
                                        "#ui-tabs-".$tabs_id);
    
} else {
    $class = "error";
    $message = _("L'element n'est pas accessible.");
    $f->addToMessage($class, $message);
    $f->setFlag(NULL);
    $f->display();
}




?>