<?php
// ===================================
// *** recuperation de variable en URL
// ===================================
// numero d objet
// obj
$option = array();
if (isset ($_GET['obj'])){
	$obj=$_GET['obj'];
}
if (isset ($_GET['idx'])){
	$options['idx']=$_GET['idx'];
	
}
// ouverture en popup
if (isset ($_GET['popup'])){
	if ($_GET['popup'] <> '')
		$options['popup']=$_GET['popup'];
}
if (isset ($_GET['min'])){
	if ($_GET['min']<>"") {
		$min =$_GET['min'];
	}else{
		$min=0;
	}
} else {
	$min=0;
}
if (isset ($_GET['max'])){
	if ($_GET['max']<>"") {
		$max =$_GET['max'];
	}else{
		$max=0;
	}
} else {
	$max=0;
}
// parametrage de l etendue dans l url 
if (isset ($_GET['etendue'])){
   $options['etendue']=$_GET['etendue'];
}
// reqmo
if (isset ($_GET['reqmo'])){
   $options['reqmo']=$_GET['reqmo'];
}
// recherche
// - premier
if (isset ($_GET['premier'])){
   $options['premier']=$_GET['premier'];
}
// - recherche
if (isset ($_GET['recherche'])){
   $options['recherche']=$_GET['recherche'];
}
// - selectioncol
if (isset ($_GET['selectioncol'])){
   $options['selectioncol']=$_GET['selectioncol'];
}
// - tricol
if (isset ($_GET['tricol'])){
   $options['tricol']=$_GET['tricol'];
}
// - advs_id
if (isset ($_GET['advs_id'])){
   $options['advs_id']=$_GET['advs_id'];
}
// - valide
if (isset ($_GET['valide'])){
   $options['valide']=$_GET['valide'];
}
// - style
if (isset ($_GET['style'])){
   $options['style']=$_GET['style'];
}
// - onglet
if (isset ($_GET['onglet'])){
   $options['onglet']=$_GET['onglet'];
}

// ==============================
// utils + librairies javascripts
// ==============================
include ("../obj/utils.class.php");
$f = new utils ('nohtml');
$f->disableLog();
if (file_exists('../obj/'.$obj.'.map.class.php')) {
    require_once '../obj/'.$obj.'.map.class.php';
    $om_map = new om_map_obj($obj, $options);
} else {
    require_once PATH_OPENMAIRIE."om_map.class.php";
    $om_map = new om_map($obj, $options);
}
$om_map->recupOmSigMap();
$geojson = explode("#", str_replace("\'","'",$_POST['geojson']));
$sep="";
$i=0;
for ($c=$min; $c<=$max;$c++) {
	if ($geojson[$i]!='') {
		echo $sep.$om_map->getComputeGeom($c, $geojson[$i]);
	}else{
		echo $sep.$geojson[$i];
	}
	$sep="#";
	$i=$i+1;
}
$om_map->__destruct();
?>