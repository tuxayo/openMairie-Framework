<?php
/**
 * Redirection vers le formulaire de l'objet en visualisation si l'objet 
 * existe et en ajout sinon
 *
 * @package openfoncier
 * @version SVN : $Id: redirection_onglet.php 1512 2013-03-19 18:18:15Z fmichon $
 */
/**
 * Fichiers requis
 */
require_once "../obj/utils.class.php";
if (file_exists("../dyn/var.inc")) {
            include "../dyn/var.inc";
        }
        
// Identifiant du dossier
$idx = (isset($_GET['idx'])) ? $_GET['idx'] : "" ;
$obj = (isset($_GET['obj'])) ? $_GET['obj'] : "" ;
if ( isset($idx) && !is_null($idx) && isset($obj) && !is_null($obj)){    
    $f = new utils("nohtml", $obj);
    $f->disableLog();
    
    $idx = explode('?', $idx);
    $idx = $idx[0];

    echo '<div id="sousform-'.$obj.'">';
		echo '<script type="text/javascript" >ajaxIt(\''.$obj.'\',\'../scr/sousform.php?objsf='.$obj.'&idxformulaire='.$idx.'&retourformulaire='.$obj.'&obj='.$obj.'&action=3&idx='.$idx.'\', 1);</script>';
    echo '</div>';
}
?>