<?php
/**
 * GEOLOCALISATION - Ce script permet ...
 *
 * @package openmairie_exemple
 * @version SVN : $Id: sig_session.php 3023 2015-02-02 08:21:32Z baldachino $
 */

require_once "../obj/utils.class.php";
$f = new utils ('nohtml');
$f->disableLog();

// Vérification de l'activation de l'option localisation
// $f->handle_if_no_localisation();

//
$obj=$f->db->escapeSimple($_POST['obj']);
$zoom=$f->db->escapeSimple($_POST['zoom']);
$base=$f->db->escapeSimple($_POST['base']);
if (isset ($_POST['visibility'])) {
    $visibility=$_POST['visibility'];
} else {
    $visibility=null;
}

if (isset ($_POST['seli'])) {
	$seli=$f->db->escapeSimple($_POST['seli']);
}else{
	$seli=0;	
}
$_SESSION['map_'.$obj]=array("zoom" => $zoom, "base" => $base, "seli" => $seli, "visibility" => $visibility);
$result='ok';
echo $result;

?>
