*** Settings ***
Resource  resources/resources.robot
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown
Documentation  TestSuite "Tableaux"...


*** Test Cases ***
Constitution du jeu de données

    [Documentation]  L'objet de ce TestCase est de constituer un jeu de
    ...  données cohérent pour les scénarios fonctionnels qui suivent.

    #
    Depuis la page d'accueil  admin  admin
    # Il faut que le listing affiche 15 résultats par page.
    ${profil}    Set Variable    PUBLIC
    Set Suite Variable    ${profil}
    Ajouter le profil depuis le menu    ${profil}    6
    # On ajoute 20 droits au profil
    :FOR    ${INDEX}    IN RANGE    0    20
    \    Ajouter le droit depuis le menu    public_${INDEX}    ${profil}
    # Puis 2 specifiques pour la recherche
    Ajouter le droit depuis le menu    recherche_d'apostrophe    ${profil}
    Ajouter le droit depuis le menu    recherche_accents_éèàç    ${profil}

    # Création d'une requête destinée à la lettre-type à copier en tableau
    ${query_tab}  Set Variable  Nonetab
    Ajouter la requête  ${query_tab}  ${query_tab}  -  sql  SELECT 1;  -
    # Création de la lettre-type à copier en tableau
    ${id_tab}  Set Variable  test_action_direct_tab
    Set Suite Variable    ${id_tab}
    Ajouter la lettre-type depuis le menu  ${id_tab}  ${id_tab}  <p>${id_tab}</p>  <p><span style="font-weight: bold;">${id_tab}</span></p>  ${query_tab}  true
    # Création d'une requête destinée à la lettre-type à copier en tableau
    ${query_soustab}  Set Variable  Nonesoustab
    Set Suite Variable    ${query_soustab}
    Ajouter la requête  ${query_soustab}  ${query_soustab}  -  sql  SELECT 1;  -
    # Création de la lettre-type à copier en tableau
    ${id_soustab}  Set Variable  test_action_direct_soustab
    Set Suite Variable    ${id_soustab}
    Ajouter la lettre-type depuis le menu  ${id_soustab}  ${id_soustab}  <p>${id_soustab}</p>  <p><span style="font-weight: bold;">${id_soustab}</span></p>  ${query_soustab}  true

Pagination en formulaire

    [Documentation]    Vérifie la pagination sur un formulaire.

    #
    Depuis la page d'accueil  admin  admin
    #
    Depuis le listing des droits
    # On récupère la plage d'enregistrement de la première page
    ${pagination_premiere_page} =    Get Pagination Text
    # On sélectionne la deuxième page
    Select Pagination    15
    # On vérifie que la page à changé avec la plage d'enregistrement
    Pagination Text Not Should Be    ${pagination_premiere_page}


Pagination en sous-formulaire

    [Documentation]    Vérifie la pagination sur un sous-formulaire.

    #
    Depuis la page d'accueil  admin  admin
    #
    Depuis le listing des droit du profil    null    ${profil}
    # On récupère la plage d'enregistrement de la première page
    ${pagination_premiere_page} =    Get Pagination Text
    # On sélectionne la deuxième page
    Select Pagination    15
    # On vérifie que la page à changé avec la plage d'enregistrement
    Pagination Text Not Should Be    ${pagination_premiere_page}


Recherche simple

    [Documentation]    Permet de tester la recherche de caractères speciaux.

    #
    Depuis la page d'accueil  admin  admin
    #
    Depuis le listing des droits
    # On recherche un terme avec apostrophe
    Use Simple Search    Tous    recherche_d'apostrophe
    # Vérification du résultat
    Elements From Column Should Be    1    recherche_d'apostrophe
    # Idem avec accents
    Use Simple Search    Tous    recherche_accents_éèàç
    # Vérification du résultat
    Elements From Column Should Be    1    recherche_accents_éèàç

Actions direct

    [Documentation]    Copie des lettres-types en tableau et sous-tableau
    ...    Une action-direct exécute le traitement et affiche son message
    ...    en rechargeant en AJAX le listing.

    # Ajout de l'action-direct "copier" dans le listing des lettres-types
    Move File  ${EXECDIR}${/}..${/}sql${/}pgsql${/}om_lettretype.inc.php  ${EXECDIR}${/}..${/}sql${/}pgsql${/}om_lettretype.inc.php.backup
    Copy File  ${EXECDIR}${/}binary_files${/}om_lettretype.inc.php  ${EXECDIR}${/}..${/}sql${/}pgsql${/}om_lettretype.inc.php
    Sleep  3

    # Copie en tableau
    Depuis le tableau des lettres-types
    Total Results In Tab Should Be Equal  4  om_lettretype
    Page Should Contain  ${id_tab}
    Page Should Contain  ${id_soustab}
    ${date_fr} =  Date du jour au format dd/mm/yyyy
    Page Should Not Contain  copie du ${date_fr}
    Click Element  action-tab-om_lettretype-left-copier-3
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Total Results In Tab Should Be Equal  5  om_lettretype
    Valid Message Should Contain In Tab  L'element a ete correctement duplique.
    Click On Link  copie du ${date_fr}
    Element Text Should Be  id  ${id_tab}

    # Copie en sous-tableau
    Depuis le tableau des requêtes
    Click On Link  ${query_soustab}
    On clique sur l'onglet  om_lettretype  lettre type
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Total Results In Subform Should Be Equal  1  om_lettretype
    Page Should Not Contain  copie du ${date_fr}
    Click Element  action-soustab-om_lettretype-left-copier-4
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Total Results In Subform Should Be Equal  2  om_lettretype
    Valid Message Should Contain In Subtab  L'element a ete correctement duplique.
    Click On Link  copie du ${date_fr}
    Element Text Should Be  id  ${id_soustab}

    # Restauration de la configuration du listing des lettres-types
    Remove File  ${EXECDIR}${/}..${/}sql${/}pgsql${/}om_lettretype.inc.php
    Move File  ${EXECDIR}${/}..${/}sql${/}pgsql${/}om_lettretype.inc.php.backup  ${EXECDIR}${/}..${/}sql${/}pgsql${/}om_lettretype.inc.php
