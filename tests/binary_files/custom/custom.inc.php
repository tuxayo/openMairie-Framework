<?php
/**
 * Ce script permet de déclarer des surcharges niveau 'CUSTOM'.
 *
 * @package openmairie_exemple
 * @version SVN : $Id$
 */

//
$custom = array(
    "tab" => array(
        "om_collectivite" => '../custom/sql/pgsql/om_collectivite.inc.php',
    ),
    "obj" => array(
        "om_logo" =>'../custom/obj/om_logo.class.php',
    ),
);

?>
