*** Settings ***
Resource  resources/resources.robot
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown
Documentation  TestSuite "Documentation"...


*** Keywords ***
Highlight heading
    [Arguments]  ${locator}
    Update element style  ${locator}  margin-top  0.75em
    Highlight  ${locator}


*** Test Cases ***
Constitution du jeu de données
    [Tags]  doc

    Depuis la page d'accueil  admin  admin
    Ajouter la requête  none  Requête SQL  Ne rend disponible aucun champ de fusion.  sql  SELECT 1;
    Ajouter le logo  logopdf.png  logopdf.png  testImportManuel.jpg  null  null  true

    &{args_lettretype} =  Create Dictionary
    ...  id=om_utilisateur
    ...  libelle=lettre aux utilisateurs
    ...  sql=Requête SQL
    ...  titre=<p style="text-align: left;"><span style="font-size: 14px;"><span style="font-family: arial;">le&nbsp;&datecourrier</span></span></p>
    ...  corps=<p style="text-align: justify;"><span style="font-size: 10px;"><span style="font-family: times;">Nous&nbsp;avons&nbsp;le&nbsp;plaisir&nbsp;de&nbsp;vous&nbsp;envoyer&nbsp;votre&nbsp;login&nbsp;et&nbsp;votre&nbsp;mot&nbsp;de&nbsp;passevotre&nbsp;login&nbsp;[login]&nbsp;Vous&nbsp;souhaitant&nbsp;bonne&nbsp;receptionVotre&nbsp;administrateur</span></span></p>
    ...  actif=true
    ...  logo=logopdf.png (logopdf.png)

    Ajouter la lettre-type depuis le menu  &{args_lettretype}


Take an annotated screenshot
    [Tags]  doc
    #
    Depuis la page d'accueil  admin  admin
    #
    Create Directory    results/screenshots
    Create Directory    results/screenshots/usage
    Create Directory    results/screenshots/usage/administration
    #
    Depuis le contexte de la lettre-type    om_utilisateur
    Click On Form Portlet Action  om_lettretype  modifier
    Click Element  css=#fieldset-form-om_lettretype-parametres-generaux-de-l_edition legend
    Sleep  1
    Click Element  css=#fieldset-form-om_lettretype-en-tete legend
    Sleep  1
    Click Element  css=#fieldset-form-om_lettretype-parametres-du-titre-de-l_edition legend
    Sleep  1
    Click Element  css=#fieldset-form-om_lettretype-parametres-des-sous-etats legend
    Sleep  1
    Click Element  css=#fieldset-form-om_lettretype-pied-de-page legend
    Sleep  1
    #
    Capture and crop page screenshot  screenshots/usage/administration/editions_etat_lettretype_bloc_edition.png
    ...    fieldset-form-om_lettretype-edition
    Capture and crop page screenshot  screenshots/usage/administration/editions_etat_lettretype_bloc_en-tete.png
    ...    fieldset-form-om_lettretype-en-tete
    Capture and crop page screenshot  screenshots/usage/administration/editions_etat_lettretype_bloc_titre.png
    ...    fieldset-form-om_lettretype-titre
    Capture and crop page screenshot  screenshots/usage/administration/editions_etat_lettretype_bloc_corps.png
    ...    fieldset-form-om_lettretype-corps
    Capture and crop page screenshot  screenshots/usage/administration/editions_etat_lettretype_bloc_pied-de-page.png
    ...    fieldset-form-om_lettretype-pied-de-page
    Capture and crop page screenshot  screenshots/usage/administration/editions_etat_lettretype_bloc_champs-de-fusion.png
    ...    fieldset-form-om_lettretype-champs-de-fusion


Capture d'ecran de l'interface des widgets
    [Tags]  doc


    Depuis la page d'accueil  admin  admin
    Go To Submenu In Menu    administration    om_widget

    Click Element    css=#action-tab-om_widget-corner-ajouter

    Select From List    css=#type    web

    Capture and crop page screenshot  screenshots/usage/administration/tableau_de_bord_widget_ajout_web.png
    ...    form-content

    Input Text    css=#libelle    widget lien
    Input Text    css=#lien    http://www.atreal.fr/
    Input Text    css=#texte    Donec sed tristique lectus. Nullam blandit leo vitae lectus suscipit dignissim. Vestibulum adipiscing nisi vel tortor tempus dignissim ac a magna. Mauris vestibulum in orci in volutpat. Interdum et malesuada fames ac ante ipsum primis in faucibus. Aliquam malesuada purus aliquet iaculis hendrerit. Phasellus sagittis sed diam ac blandit. Proin molestie justo vel velit imperdiet, a congue sem egestas. Integer id nibh volutpat felis interdum pretium.
    Click On Submit Button
    Valid Message Should Be    Vos modifications ont bien été enregistrées.
    Click On Back Button

    Click Element    css=#action-tab-om_widget-corner-ajouter
    Select From List    css=#type    file


    Capture and crop page screenshot  screenshots/usage/administration/tableau_de_bord_widget_ajout_file.png
    ...    form-content


    Input Text    css=#libelle    widget file
    Select From List    css=#script    test_robotframework
    Input Text    css=#arguments    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer facilisis risus id turpis eleifend, sed facilisis lectus congue. Nulla mattis ultricies euismod. Praesent faucibus in ipsum at sodales. Maecenas lectus massa, dapibus ut tortor ac, viverra egestas mauris. Morbi mi elit, ullamcorper sed tincidunt nec, fermentum sed nisi. Mauris a feugiat nisl. Maecenas nunc lorem, vehicula eu fermentum non, ullamcorper sed eros. Phasellus porttitor massa nec nisi facilisis, non pulvinar enim ullamcorper. Cras ac ante luctus, fringilla enim sed, malesuada elit. Nunc ultricies, dui non sollicitudin accumsan, diam purus porttitor sem, rhoncus placerat ante quam vel nisl. Nam adipiscing mauris risus, id iaculis est volutpat eget. Curabitur tortor lacus, pharetra ultricies tristique eu, consequat et odio. Morbi vestibulum nec lorem quis luctus. Etiam non varius quam. Ut vehicula, neque vel blandit malesuada, nisi nunc dignissim odio, et pellentesque dolor augue ac ipsum.
    Click On Submit Button
    Valid Message Should Be    Vos modifications ont bien été enregistrées.
    Click On Back Button


    Capture and crop page screenshot  screenshots/usage/administration/tableau_de_bord_widget_liste.png
    ...    formulaire
