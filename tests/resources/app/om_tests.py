#!/usr/bin/python
# -*- coding: utf-8 -*-
from resources.core.om_tests import om_tests_core


class om_tests(om_tests_core):
    """
    """

    _params_copy_files = [
        {'in': 'tests/binary_files/dyn/*.inc.php', 'out': 'dyn/'},
        {'in': 'tests/binary_files/sql/pgsql/*inc.php', 'out': 'sql/pgsql/'},
        {'in': 'tests/binary_files/storage/*', 'out': 'var/filestorage/'},
        {'in': 'tests/binary_files/htaccess_deny_from_all', 'out': 'var/.htaccess'},
        {'in': 'tests/binary_files/widget_test_robotframework.php', 'out': 'app/widget_test_robotframework.php'},
    ]

    _params_create_folders = [
        'var/',
        'var/filestorage/',
        'var/tmp/',
        'var/log/',
    ]

    _params_chmod_777 = [
        # apache doit pouvoir écrire dans les répertoires de storage et de log
        'var/',
        # apache doit pouvoir écrire dans les répertoires destinés à recevoir 
        # des fichiers générés
        'gen/obj/', 'gen/sql/pgsql/', 
        'core/obj/', 'core/sql/pgsql/',
        'obj/', 'sql/pgsql/',
        'tests/resources/core/gen/',
        # apache doit pouvoir écrire dans les répertoires de configuration
        # afin de tester les paramètres
        'dyn/',
    ]

