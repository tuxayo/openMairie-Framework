*** Settings ***
Documentation    CRUD de la table om_logo
...    @author  generated
...    @package openExemple
...    @version 26/06/2015 17:06

*** Keywords ***
Depuis le listing des logos
    [Tags]
    Go To Tab  om_logo

Depuis le contexte du logo
    [Tags]
    [Documentation]  Accède au formulaire
    [Arguments]    ${om_logo}

    # On accède au tableau
    Depuis le listing des logos
    # On recherche l'enregistrement
    Use Simple Search    Tous    ${om_logo}
    # On clique sur le résultat
    Click On Link    ${om_logo}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter le logo
    [Tags]
    [Documentation]  Crée l'enregistrement
    [Arguments]    ${id}    ${libelle}    ${fichier}    ${description}=null    ${resolution}=null    ${actif}=null    ${om_collectivite}=null

    # On accède au tableau
    Go To Tab    om_logo
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir le logo    ${id}    ${libelle}    ${fichier}    ${description}    ${resolution}    ${actif}    ${om_collectivite}
    # On valide le formulaire
    Click On Submit Button
    # On vérifie le message de validation
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.


Modifier le logo
    [Tags]
    [Documentation]  Modifie l'enregistrement
    [Arguments]    ${id}=null    ${libelle}=null    ${fichier}=null   ${description}=null    ${resolution}=null    ${actif}=null    ${om_collectivite}=null

    # On accède à l'enregistrement
    Depuis le contexte du logo    ${id}
    # On clique sur le bouton modifier
    Click On Form Portlet Action    om_logo    modifier
    # On saisit des valeurs
    Saisir le logo    ${id}    ${libelle}    ${fichier}    ${description}    ${resolution}    ${actif}    ${om_collectivite}
    # On valide le formulaire
    Click On Submit Button
    # On vérifie le message de validation
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.

Supprimer le logo
    [Tags]
    [Documentation]  Supprime l'enregistrement
    [Arguments]    ${logo}

    # On accède à l'enregistrement
    Depuis le contexte du logo    ${logo}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action    om_logo    supprimer
    # On valide le formulaire
    Click On Submit Button
    # On vérifie le message de validation
    Valid Message Should Contain  La suppression a été correctement effectuée.

Saisir le logo
    [Tags]
    [Documentation]  Remplit le formulaire
    [Arguments]    ${id}=null    ${libelle}=null    ${fichier}=null    ${description}=null    ${resolution}=null    ${actif}=null    ${om_collectivite}=null

    Run Keyword If  '${id}' != 'null'  Input Text    id    ${id}
    Run Keyword If  '${libelle}' != 'null'  Input Text    libelle    ${libelle}
    Run Keyword If  '${description}' != 'null'  Input Text    description    ${description}
    Run Keyword If  '${fichier}' != 'null'  Add File    fichier    ${fichier}
    Run Keyword If  '${resolution}' != 'null'  Input Text    resolution    ${resolution}
    Run Keyword If  '${actif}' != 'null'  Select Checkbox    actif
    Run Keyword If  '${om_collectivite}' != 'null'  Select From List By Value    om_collectivite    ${om_collectivite}
