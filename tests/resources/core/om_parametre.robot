*** Settings ***
Documentation  Actions spécifiques aux paramètres.

*** Keywords ***
Depuis le tableau des paramètres
    [Tags]
    [Documentation]  Permet d'accéder au tableau des paramètres.

    # On ouvre le tableau
    Go To Tab  om_parametre


Depuis le contexte du paramètre
    [Tags]
    [Documentation]  Permet d'accéder au formulaire en consultation
    ...    d'un paramètre.
    [Arguments]  ${libelle}=null  ${valeur}=null

    # On ouvre le tableau des paramètres
    Depuis le tableau des paramètres
    # On recherche le paramètre
    Run Keyword If    '${valeur}' != 'null'    Use Simple Search    valeur    ${valeur}    ELSE IF    '${libelle}' != 'null'    Use Simple Search    libellé    ${libelle}    ELSE    Fail
    # On clique sur le paramètre
    Run Keyword If    '${valeur}' != 'null'    Click On Link    ${valeur}    ELSE IF    '${libelle}' != 'null'    Click On Link    ${libelle}    ELSE    Fail


Ajouter le paramètre depuis le menu
    [Tags]
    [Documentation]  Permet d'ajouter un paramètre
    [Arguments]  ${libelle}  ${valeur}  ${collectivite}

    # On ouvre le tableau des paramètres
    Depuis le tableau des paramètres
    # On clique sur l'icone d'ajout
    Click On Add Button
    # On remplit le formulaire
    Saisir le paramètre  ${libelle}  ${valeur}  ${collectivite}
    # On valide le formulaire
    Click On Submit Button
    # On vérifie le message de validation
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.

Modifier le paramètre
    [Tags]
    [Documentation]  Permet de modifier un paramètre
    [Arguments]  ${libelle}  ${valeur}  ${collectivite}=null

    # On ouvre le tableau des paramètres
    Depuis le tableau des paramètres
    # On recherche puis on clique sur le paramètre souhaité
    Depuis le contexte du paramètre  ${libelle}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  om_parametre  modifier
    # On remplit le formulaire
    Saisir le paramètre  ${libelle}  ${valeur}  ${collectivite}
    # On valide le formulaire
    Click On Submit Button
    # On vérifie le message de validation
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.

Saisir le paramètre
    [Tags]
    [Documentation]  Permet de remplir le formulaire d'un paramètre.
    [Arguments]  ${libelle}  ${valeur}  ${collectivite}

    # On saisit le libellé
    Input Text  libelle  ${libelle}
    # On saisit la valeur
    Input Text  valeur  ${valeur}
    # On sélectionne la collectivité si définie
    Run Keyword If  '${collectivite}' != 'null'  Select From List By Label  om_collectivite  ${collectivite}

Supprimer le paramètre
    [Tags]
    [Documentation]  Permet de supprimer le paramètre
    [Arguments]  ${libelle}  ${valeur}

    # On accède à l'enregistrement
    Depuis le contexte du paramètre  ${libelle}  ${valeur}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  om_parametre  supprimer
    # On valide le formulaire
    Click On Submit Button
    # On vérifie le message de validation
    Valid Message Should Contain  La suppression a été correctement effectuée.
