*** Settings ***
Documentation     Ressources (librairies, ressources, variables et keywords)

# Librairies
Library           Collections
Library           OperatingSystem
Library           RequestsLibrary
Library           Selenium2Library
Library           String
Library           Selenium2Screenshots
Library           ArchiveLibrary
Library           DateTime

# Mots-clefs Framework
Resource          formulaire.robot
Resource          menu.robot
Resource          navigation.robot
Resource          pdf.robot
Resource          tableau.robot
Resource          utils.robot
Resource          module_gen.robot
Resource          module_reqmo.robot
Resource          module_import.robot
Resource          module_sig_interne.robot

# Mots-clefs objet Framework
Resource          om_collectivite.robot
Resource          om_droit.robot
Resource          om_requete.robot
Resource          om_lettretype.robot
Resource          om_parametre.robot
Resource          om_profil.robot
Resource          om_sousetat.robot
Resource          om_utilisateur.robot
Resource          om_widget.robot
Resource          om_etat.robot
Resource          om_logo.robot

*** Variable ***
${TIMEOUT}         20 sec
${RETRY_INTERVAL}  0.2 sec

*** Keywords ***
Tests Setup
    [Tags]  om_resources
    # Définit les variables globales dates du jour
    ${DATE_DDMMYYYY} =  Date du jour FR
    ${DATE_YYYYMMDD} =  Date du jour EN
    ${DATE_FORMAT_YYYY-MM-DD} =  Date du jour au format yyyy-mm-dd
    ${DATE_FORMAT_YYYYMMDD} =  Date du jour au format yyyymmdd
    ${DATE_FORMAT_DD/MM/YYYY} =  Date du jour au format dd/mm/yyyy
    Set Global Variable  ${DATE_DDMMYYYY}
    Set Global Variable  ${DATE_YYYYMMDD}
    Set Global Variable  ${DATE_FORMAT_YYYY-MM-DD}
    Set Global Variable  ${DATE_FORMAT_YYYYMMDD}
    Set Global Variable  ${DATE_FORMAT_DD/MM/YYYY}

